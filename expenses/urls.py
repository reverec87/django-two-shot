from django.contrib import admin
from django.urls import include, path
from django.shortcuts import redirect


def redirect_home(request):
    return redirect("home")


urlpatterns = [
    path('', redirect_home, name='home'),
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("accounts/", include("accounts.urls")),
]
